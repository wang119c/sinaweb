<?php
/**
 * Created by PhpStorm.
 * User: luodongyun
 * Date: 2017/3/21
 * Time: 下午11:23
 */

session_start();
unset($_SESSION['token']);
header("Location: index.php");