<?php
/**
 * Created by PhpStorm.
 * User: luodongyun
 * Date: 2017/3/21
 * Time: 下午9:45
 */
require("config.php");
require("saetv2.ex.class.php");
$o = new SaeTOAuthV2(WB_AKEY, WB_SKEY);
$code_url = $o->getAuthorizeURL(WB_CALLBACK_URL);
header("Location: " . $code_url);